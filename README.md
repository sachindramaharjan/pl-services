#Project Setup
https://medium.freecodecamp.org/structuring-a-flask-restplus-web-service-for-production-builds-c2ec676de563

#Migrate Database - Run below commands when there is change in database

1. Initialize
    python3 manage.py db init

2. Create migration script
    python3 manage.py db migrate --message 'initial database migration'

3. Apply script to migrate db
    python3 manage.py db upgrade
