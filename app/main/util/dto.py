from flask_restplus import Namespace, fields


class UserDto:
    user_api = Namespace('user', 'user related operations')
    user = user_api.model('user', {
        'email': fields.String(required=True, description='Email'),
        'username': fields.String(required=True, description='Username'),
        'password': fields.String(required=True, description='Password')
        }
    )

