import datetime

from sqlalchemy import or_
from app.main.model.user import User
from app.main import db


def save_new_user(data):
    user = db.session.query(User).filter(or_(User.username == data['username'], User.email == data['email'])).first()
    if not user:
        new_user = User(username=data['username'],
                        email=data['email'],
                        password=data['password'],
                        registered_on=datetime.datetime.utcnow())
        save(new_user)
        response_object = {
            'status': 'success',
            'message': 'Successfully registered'
        }
        return response_object, 201
    else:
        response_object = {
            'status': 'failed',
            'message': 'User already registered. Please log in'
        }
        return response_object, 409


def get_all_users():
    return User.query.all()


def get_user_by_id(user_id):
    return User.query.filter_by(id=user_id).first()


def get_user_by_username_or_email(username_or_email):
    return User.query.filter_by(username=username_or_email).first()


def save(data):
    db.session.add(data)
    db.session.commit()
