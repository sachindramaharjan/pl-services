from .. import db, flask_bcrypt


class User(db.Model):
    """User model for storing user information"""

    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(255), unique=True, nullable=False)
    email = db.Column(db.String(255), unique=True, nullable=False)
    admin = db.Column(db.Boolean, nullable=False, default=False)
    password = db.Column(db.String(255))
    registered_on = db.Column(db.DateTime, nullable=False)

    def __init__(self, username, email, password, registered_on):
        self.username = username
        self.email = email
        self.password = flask_bcrypt.generate_password_hash(password).decode('utf-8')
        self.registered_on = registered_on

    # @property
    # def password(self):
    #     raise AttributeError('Password is write-only')
    #
    # @password.setter
    # def password(self, password):
    #     self.password = flask_bcrypt.generate_password_hash(password).decode('utf-8')
    #
    def check_password(self, password):
        return flask_bcrypt.check_password_hash(self.password, password)

    def __repr__(self):
        return '<User {}>'.format(self.username)
