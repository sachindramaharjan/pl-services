from flask import Flask, request
from flask_restplus import Resource

from app.main.util.dto import UserDto
from app.main.service import user_service


api = UserDto.user_api
_user = UserDto.user


@api.route('/')
class UserList(Resource):
    @api.doc('list of registered users')
    @api.marshal_list_with(_user, envelope='data')
    def get(self):
        """Returns all registered users"""
        return user_service.get_all_users()

    @api.response(201, 'User successfully created')
    @api.doc('register a new user')
    @api.expect(_user, validate=True)
    def post(self):
        """Save a new user"""
        data = request.json
        return user_service.save_new_user(data=data)


@api.route('/<user_id>')
@api.param('user_id', 'The user identification')
@api.response(404, 'User not found')
class User(Resource):
    @api.doc('Get a user by id')
    @api.marshal_with(_user)
    def get_by_usename_or_email(self, username_or_email):
        """Get a user by id"""
        user = user_service.get_user_by_id(username_or_email)
        if not user:
            api.abort(404, 'User not found')
        else:
            return user
